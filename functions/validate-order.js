
const DocumentClient = require('aws-sdk/clients/dynamodb').DocumentClient
const dynamodb = new DocumentClient()
const { SQS, S3 } = require('aws-sdk')
const sqs = new SQS()
const s3 = new S3()

const tableName = process.env.orders_table
const ordersQueue = process.env.orders_queue
const ordersBucket = process.env.orders_bucket

module.exports.handler = async (event, context) => {
  
  console.log(event.body)

  const { outboundOrderNumber: orderNumber, clientId: clientID, warehouseId: warehouseID } = JSON.parse(event.body);

  let s3Filename = clientID + "/" + orderNumber + ".json";

  const s3Payload = {
    Bucket: ordersBucket,
    Key: s3Filename, // File name you want to save as in S3
    Body: event.body
  };
  
  let s3Response = await s3.upload(s3Payload).promise();
  
  console.log(s3Response);
  let fileLocation = s3Response.Location;
  let fileKey = s3Response.Key;

  
  console.log('put to s3 complete')
  
  console.log(`putting order ${orderNumber} into ${tableName}...`)
  
  const req = {
    TableName: tableName,
    Item: {
      "orderNumber": orderNumber,
      "clientID": clientID,
      "warehouseID": warehouseID,
      "fileLocation": fileLocation,
      "orderStatus": "NEW"
    }
  }

  await dynamodb.put(req).promise()

  console.log('dynamo update complete')
  
  const sqsPayload = {
    MessageBody: JSON.stringify({
      "orderNumber": orderNumber,
      "clientID": clientID,
      "warehouseID": warehouseID,
      "fileLocation": fileLocation,
      "fileKey": fileKey
    }),
    QueueUrl: ordersQueue
  };
  
  console.log('putting to sqs')

  await sqs.sendMessage(sqsPayload).promise()

  console.log('put to sqs complete')

  const response = {
    statusCode: 200,
    body: JSON.stringify({ orderNumber })
  }

  return response
}
