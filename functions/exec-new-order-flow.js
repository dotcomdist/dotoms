const { StepFunctions } = require('aws-sdk');
const stepfunctions = new StepFunctions();

const new_orders_state_machine = process.env.new_orders_state_machine;

module.exports.handler = async (event, context) => {
    
  for (let recordIndex in event.Records){
    
    const record = event.Records[recordIndex];
    const { body } = record;
    
    const params = {
      stateMachineArn: new_orders_state_machine,
      input: body
    };

    await stepfunctions.startExecution(params).promise();
    console.log('started execution of step function');
    
  };
};