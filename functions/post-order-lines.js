const axios = require('axios');
const {
  getToken,
  refreshToken,
  postToWMS
} = require('../utilities/util');
const {
  S3
} = require('aws-sdk');
const s3 = new S3();

const ordersBucket = process.env.orders_bucket;

module.exports.handler = async (event, context) => {

  let {
    orderNumber,
    clientID,
    warehouseID,
    params
  } = event;

  console.log(params);

  let response = await s3.getObject(params).promise();

  let url = 'http://api-dev.dotcomdistribution.com:4605/ws/wm/orderLines';

  console.log('response.Body:' + response.Body);
  let { orderLines: linesPayload } = JSON.parse(response.Body);
  let status = null;
  console.log('linesPayload: ' + linesPayload);
  for (let line of linesPayload) {

    console.log('inside post-order-lines, line:' + line);

    let retry = true;
    let retryCount = 0;
    
    while (retry == true && retryCount < 2) {

      console.log("order lines payload:" + line);
      let orderLinePost = await postToWMS(url, line);

      status = orderLinePost.status;

      console.log('orderLinePost:' + orderLinePost);
      console.log('order line post status: ' + status);

      switch (status) {
        case 201:
          console.log('Post Successful');
          retry = false;
          break;

        case 401:
          console.log('401 Error. Retrying...');
          retryCount++;
          break;

        default:
          retry = false;
          throw new Error('Order Post Failed');

      }

    }
  }

  return {
    orderNumber,
    clientID,
    warehouseID,
    status,
    params
  };

};