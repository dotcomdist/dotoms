const axios = require('axios');
const {
  getToken,
  refreshToken,
  postToWMS
} = require('../utilities/util');
const {
  S3
} = require('aws-sdk');
const s3 = new S3();

const ordersBucket = process.env.orders_bucket;

module.exports.handler = async (event, context) => {

  let {
    orderNumber,
    clientID,
    warehouseID,
    params
  } = event;

  let orderPK = clientID + "*!" + orderNumber + "*!" + warehouseID;

  let url = 'http://api-dev.dotcomdistribution.com:4605/ws/wm/outboundOrders/' + orderPK;

  console.log('inside remove-order, url:' + url);

  let retry = true;
  let retryCount = 0;
  let status = null;

  while (retry == true && retryCount < 2) {

    let orderRemove = await postToWMS(url, null, 'DELETE');

    status = orderRemove.status;

    console.log('orderRemove:' + orderRemove);
    console.log('orderRemove status: ' + status);

    switch (status) {
      case 200:
        console.log('Remove Successful');
        retry = false;
        break;

      case 401:
        console.log('401 Error. Retrying...');
        retryCount++;
        break;

      default:
        retry = false;
        throw new Error('Order Removal Failed');

    }

  }

  return {
    orderNumber,
    clientID,
    warehouseID,
    status,
    params
  };

};