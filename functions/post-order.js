const axios = require('axios');
const {
  getToken,
  refreshToken,
  postToWMS
} = require('../utilities/util');
const {
  S3
} = require('aws-sdk');
const s3 = new S3();

const ordersBucket = process.env.orders_bucket;

module.exports.handler = async (event, context) => {

  let {
    orderNumber,
    clientID,
    warehouseID,
    fileLocation,
    fileKey
  } = event;

  const params = {

    Bucket: ordersBucket,
    Key: fileKey

  };

  console.log(params);

  let response = await s3.getObject(params).promise();
  let orderPayload = response.Body.toString();
  let url = 'http://api-dev.dotcomdistribution.com:4605/ws/wm/outboundOrders';

  console.log('inside post-order, event:' + event);

  let retry = true;
  let retryCount = 0;
  let status = null;

  while (retry == true && retryCount < 2) {

    console.log("order payload:" + orderPayload);
    let orderPost = await postToWMS(url, orderPayload);

    status = orderPost.status;

    console.log('orderpost:' + orderPost);
    console.log('order post status: ' + status);

    switch (status) {
      case 201:
        console.log('Post Successful');
        retry = false;
        break;

      case 401:
        console.log('401 Error. Retrying...');
        retryCount++;
        break;

      default:
        retry = false;
        throw new Error('Order Post Failed');

    }

  }

  return {
    orderNumber,
    clientID,
    warehouseID,
    status,
    params
  };

};