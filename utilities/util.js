const axios = require('axios');
const {
    DocumentClient
} = require('aws-sdk/clients/dynamodb');

const dynamodb = new DocumentClient();

const {
    session_table: sessionTable
} = process.env;

const usr_id = "LAMBDA";
const password = "Lambda123";

let cachedSessionToken = null;

const getToken = async () => {
    // I don't think this caching is necessary for Dynamo get, but doesn't hurt to add
    if (!cachedSessionToken) {
        const data = await dynamodb.get({
            TableName: sessionTable,
            Key: {
                TokenKey: 'TokenKey'
            },
        }).promise();

        cachedSessionToken = data.Item?.TokenCookie;
    }

    return cachedSessionToken;
};

const refreshToken = async () => {
    // make your call here to get cookie, should come back in set-cookie header

    const result = await axios.post('http://api-dev.dotcomdistribution.com:4605/ws/auth/login', {
        "usr_id": usr_id,
        "password": password
    });

    const tokenCookie = result?.headers?.['set-cookie'];
    console.log('tokenCookie:' + tokenCookie);

    await dynamodb.put({
        TableName: sessionTable,
        Item: {
            TokenKey: 'TokenKey',
            TokenCookie: tokenCookie,
        }
    }).promise();

    cachedSessionToken = tokenCookie;

    return cachedSessionToken;
};

const postToWMS = async (url, req, method = 'POST') => {

    let sessionToken = await getToken();

    console.log('got session token: ' + sessionToken);

    // refresh the token if it's falsy
    if (!sessionToken) {
        sessionToken = await refreshToken();
        console.log('refreshed session token: ' + sessionToken);
    }

    let config = {

        headers: {
            'Cookie': sessionToken,
            'Content-Type': 'application/json'
        },
        withCredentials: true
    };

    console.log('postToWMS - post url: ' + url);
    console.log('postToWMS - post req: ' + req);

    let result;

    if (method === 'DELETE'){

        result = await axios.delete(url, config);

    }
    else{

        result = await axios.post(url, req, config);

    }
    
    return result;
};

module.exports = {
    getToken,
    refreshToken,
    postToWMS
}