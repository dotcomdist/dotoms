const APP_ROOT = '../../'
const _ = require('lodash')

const viaHandler = async (event, functionName) => {
  const handler = require(`${APP_ROOT}/functions/${functionName}`).handler

  const context = {}
  const response = await handler(event, context)
  const contentType = _.get(
    response,
    'headers.Content-Type',
    'application/json'
  )
  if (response.body && contentType === 'application/json') {
    response.body = JSON.parse(response.body)
  }
  return response
}

const we_invoke_validate_order = (event) => viaHandler(event, 'validate-order');
const we_invoke_post_order = (event) => viaHandler(event, 'post-order');
const we_invoke_post_order_lines = (event) => viaHandler(event, 'post-order-lines');

module.exports = {
  we_invoke_validate_order,
  we_invoke_post_order,
  we_invoke_post_order_lines
}