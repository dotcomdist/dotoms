const when = require('../steps/when')
const { init } = require('../steps/init')

describe(`When we invoke the POST / endpoint`, () => {
  beforeAll(async () => await init())
  it(`Should return status 200 with the order number created`, async () => {
    
    console.log('process.env:' + process.env.orders_table)
    const payload = {"body": '{"outboundOrderNumber" : "ORD001", "clientId" : "BBY" }'}
    const res = await when.we_invoke_validate_order(payload)

    expect(res.statusCode).toEqual(200)
    //expect(res.headers['Content-Type']).toEqual('application/json')
    expect(res.body).toBeDefined()

    const orderNumber = res.body.orderNumber
    expect(orderNumber).toBeDefined()
  })
})