const when = require('../steps/when')
const {
  init
} = require('../steps/init')
const chance = require('chance').Chance()
const orderId = chance.guid()
const { S3 } = require('aws-sdk')
const s3 = new S3()

describe(`When we invoke the POST / endpoint`, () => {
  jest.setTimeout(30000)
  beforeAll(async () => await init())
  it(`Should return a status`, async () => {

    console.log('process.env:' + process.env.orders_table)
    
    const orderNumber = chance.string({ length: 5 });
    const fileKey = 'BBY/' + orderNumber + '.json';
    
    const s3Payload = {
      Bucket: process.env.orders_bucket,
      Key: fileKey, // File name you want to save as in S3
      Body: '{ "orderLines" : [{"carrier" : "UPS", "clientId" : "BBY", "hostOrderedQuantity" : 1, "inventoryStatusProgression" : "A", "itemClientId" : "BBY", "itemNumber" : "9999", "orderNumber" : "' + orderNumber + '", "orderLine" : "0001", "orderSubLine" : "0000", "orderedQuantity" : 1, "pickQuantity" : 1, "reserveQuantity" : 1, "serviceLevel" : "03", "warehouseId" : "NJ01"}]}'
    };
    
    let s3Response = await s3.upload(s3Payload).promise();
    
    console.log(s3Response);
    
    console.log(s3Payload);
    const postRes = await when.we_invoke_post_order_lines(s3Payload);

    expect(postRes.status).toBeDefined();
    //expect(res.headers['Content-Type']).toEqual('application/json')

  })
})