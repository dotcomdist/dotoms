const when = require('../steps/when')
const {
  init
} = require('../steps/init')
const chance = require('chance').Chance()
const orderId = chance.guid()
const { S3 } = require('aws-sdk')
const s3 = new S3()

describe(`When we invoke the POST / endpoint`, () => {
  jest.setTimeout(30000)
  beforeAll(async () => await init())
  it(`Should return status 201 with the order number created`, async () => {

    console.log('process.env:' + process.env.orders_table)
    
    const orderNumber = chance.string({ length: 5 });
    const fileKey = 'BBY/' + orderNumber + '.json';
    
    const s3Payload = {
      Bucket: process.env.orders_bucket,
      Key: fileKey, // File name you want to save as in S3
      Body: '{"clientId": "BBY","outboundOrderNumber": "' + orderNumber + '", "outboundOrderType": "BBY01", "billToCustomer": "BBYBT1C00333043","routeToCustomer": "BBYRT1C00333043","shipToCustomer": "BBYST1C00333043","warehouseId": "NJ01"}'
    };
    
    let s3Response = await s3.upload(s3Payload).promise();
    
    console.log(s3Response);
    
    const postPayload = {
          fileKey
    }
    console.log(postPayload);
    const postRes = await when.we_invoke_post_order(postPayload);

    expect(postRes.status).toEqual(201)
    //expect(res.headers['Content-Type']).toEqual('application/json')

  })
})