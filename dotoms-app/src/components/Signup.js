import { useState } from "react";
import UserPool from "./UserPool";
import { CognitoUserAttribute } from "amazon-cognito-identity-js";

const Signup = () => {
    const [username, setUserName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [familyname, setFamilyName] = useState("");
    const [givenname, setGivenName] = useState("");

    let attributeList = [];

    let familyNameData = {
        Name: 'family_name'
    };

    let givenNameData = {
        Name: 'given_name'
    };

    let emailData = {
        Name: 'email'
    };

    const onSubmit = (event) => {
        event.preventDefault();

        let givenNameAtt = new CognitoUserAttribute(givenNameData);
        let familyNameAtt = new CognitoUserAttribute(familyNameData);
        let emailAtt = new CognitoUserAttribute(emailData);
        attributeList.push(givenNameAtt);
        attributeList.push(familyNameAtt);
        attributeList.push(emailAtt);

        UserPool.signUp(username, password, attributeList, null, (err, data) => {
            if (err) {
                window.alert(JSON.stringify(err.message));
            }
            console.log(data);
        });
    };

    return (
        <div className="d-flex flex-column min-vh-100 justify-content-center align-items-center">
            <form className = "card" onSubmit={onSubmit}>
                <div className = "form-group">
                  <label htmlFor="username">User Name </label>
                  <input value={username} onChange={(event) => setUserName(event.target.value)}></input>
                </div>
                <div className = "form-group">
                  <label htmlFor="email">Email </label>
                  <input type="email" value={email} onChange={(event) => {setEmail(event.target.value); emailData.Value = event.target.value;}}></input>
                </div>
                <div className = "form-group">
                  <label htmlFor="password">Password </label>
                  <input type="password" value={password} onChange={(event) => setPassword(event.target.value)}></input>
                </div>
                <div className = "form-group">
                  <label htmlFor="givenname">First Name </label>
                  <input value={givenname} onChange={(event) => {setGivenName(event.target.value); givenNameData.Value = event.target.value;}}></input>
                </div>
                <div className = "form-group">
                  <label htmlFor="familyname">Last Name </label>
                  <input value={familyname} onChange={(event) => {setFamilyName(event.target.value); familyNameData.Value = event.target.value;}}></input>
                </div>
                <button className ="btn btn-primary" type="submit">Signup</button>
            </form>
        </div>
    );
};

export default Signup;