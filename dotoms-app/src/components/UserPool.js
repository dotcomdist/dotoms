import { CognitoUserPool } from "amazon-cognito-identity-js";

const poolData = {
    UserPoolId: "us-east-1_m3VckxhA9",
    ClientId: "5fmfqdjetpnh7tnqdm6ffhldp"
}

export default new CognitoUserPool(poolData);