import { useState } from "react";
import UserPool from "./UserPool";
import { CognitoUser, AuthenticationDetails } from "amazon-cognito-identity-js";

const Login = () => {
    const [username, setUserName] = useState("");
    const [password, setPassword] = useState("");

    const onSubmit = (event) => {
        event.preventDefault();

        const user = new CognitoUser({
            Username: username,
            Pool: UserPool,
        });

        const authDetails = new AuthenticationDetails({
            Username: username,
            Password: password,
        });

        user.authenticateUser(authDetails, {
            onSuccess: (data) => {
              console.log("onSuccess: ", data);
            },
            onFailure: (err) => {
              console.error("onFailure: ", err);
            },
            newPasswordRequired: (data) => {
              console.log("newPasswordRequired: ", data);
            },
        });

    };

    return (
        <div className="d-flex flex-column min-vh-100 justify-content-center align-items-center">
            <form className = "card" onSubmit={onSubmit}>
                <div className = "form-group">
                  <label htmlFor="username">User Name </label>
                  <input value={username} onChange={(event) => setUserName(event.target.value)}></input>
                </div>
                <div className = "form-group">
                  <label htmlFor="password">Password </label>
                  <input type="password" value={password} onChange={(event) => setPassword(event.target.value)}></input>
                </div>
                <button className ="btn btn-primary" type="submit">Login</button>
            </form>
        </div>
    );
};

export default Login;