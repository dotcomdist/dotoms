import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import logo from './../assets/img/dotcom-logo.png';


const Header = () => {
    return (
        <div>
          <Navbar bg="dark" variant = "dark" expand="lg">
            <Navbar.Brand href="#home">
              <img
                  src={logo}
                  className="d-inline-block align-top"
                  alt=""
              />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="#home">Home</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </div>
    )
}

export default Header;
